 //---------------setar a vez inicial e os textos-----------//
 var vez = 0 //variável pela qual será definida de quem será a vez
 njogadas = 0 //n jogadas para definir o empate
 var jaganhou = false // definição de quando parar o jogo

 //----------------------set inicial dos textos do topo---------// 
 document.getElementById("indicadorVencedor").style.display= "none"
 document.getElementById("indicadorDaVez").innerHTML = "É a vez do X"
 
 //------setar a matriz com valores aleatórios e diferentes--------//
 var linha1=["1","2","3"] 
 var linha2=["4","5","6"]
 var linha3=["7","8","9"] 


// Realiza uma jogada
function selecionar(botao) {

    if(njogadas < 9 && !jaganhou){ // verifica se já há ganhador ou se foi empate
        if(botao.innerHTML != "X" && botao.innerHTML != "O"){// verifica se a casa já foi preenchida
        
            if(vez == 0){ //vez do x
                var i = 0 // variavel de atualização da matriz
                botao.innerHTML ="X" //marca o X no html
                document.getElementById("indicadorDaVez").innerHTML = "É a vez do O" // mostra de quem é a vez
            //------------att infos na matriz a cada nova info--------------------//
                while(i < 3){
                    if(document.getElementsByClassName("casa")[i].innerHTML != ""){
                        linha1[i] = document.getElementsByClassName("casa")[i].innerHTML 
                    } //att da linha1
                    i++
                }
                while(i < 6){
                    if(document.getElementsByClassName("casa")[i].innerHTML !=""){
                        linha2[i-3] = document.getElementsByClassName("casa")[i].innerHTML            
                    } //att da linha2
                    i++
                }
                while(i < 9){
                    if(document.getElementsByClassName("casa")[i].innerHTML !=""){
                        linha3[i-6] = document.getElementsByClassName("casa")[i].innerHTML    
                    } //att da linha3
                    i++
                }
                    
                if(ganhou()){// verificação de ganhador
                    //----------set de vitoria dos textos do topo-------------//
                    document.getElementById("indicadorVencedor").style.display= "block"
                    document.getElementById("indicadorDaVez").style.display= "none"
                    document.getElementById("indicadorVencedor").innerHTML ="Vitoria do X"
                    jaganhou = true // estabelece que há um vencedor 
                }
            }
            if(vez == 1){ //vez do O
                    var i = 0 // variavel de atualização da matriz
                    botao.innerHTML = "O" //marca o O no html
                    document.getElementById("indicadorDaVez").innerHTML = "É a vez do X" // mostra de quem é a vez
                //------------att infos na matriz---------------------//
                while(i < 3){
                    if(document.getElementsByClassName("casa")[i].innerHTML !=""){
                        linha1[i] = document.getElementsByClassName("casa")[i].innerHTML
                        
                    }//att da linha1
                    i++
                }
                while(i < 6){
                    if(document.getElementsByClassName("casa")[i].innerHTML !=""){
                        linha2[i-3] = document.getElementsByClassName("casa")[i].innerHTML
                        
                    }//att da linha2
                    i++
                }
                while(i < 9){
                    if(document.getElementsByClassName("casa")[i].innerHTML !=""){
                        linha3[i-6] = document.getElementsByClassName("casa")[i].innerHTML
                        
                    }//att da linha3
                    i++
                }
        
                if(ganhou()){// verificação de ganhador
                    //----------set de vitoria dos textos do topo-------------//
                    document.getElementById("indicadorVencedor").style.display= "block"
                    document.getElementById("indicadorDaVez").style.display= "none"
                    document.getElementById("indicadorVencedor").innerHTML ="Vitoria do O"
                    jaganhou = true // estabelece que há um vencedor 
                }
            }

        njogadas++ //aumenta o numero de jogadas computadas
        }
        
    }
    
    if(vez == 1){
        vez = 0 //passa a vez para o outro jogador
    }
    else{
        vez = 1 //passa a vez para o outro jogador
    }
    if(njogadas == 9 && !jaganhou ){ // verificação de empate
        //----------set de empate dos textos do topo-------------//
        document.getElementById("indicadorVencedor").style.display= "block"
        document.getElementById("indicadorDaVez").style.display= "none"
        document.getElementById("indicadorVencedor").innerHTML ="VELHA"
        jaganhou = true//impede de o código continuar
    }
}

// Zera todos as posições e recomeça o jogo
function resetar(){
    var i //variável de iteração
    for( i=0 ; i < 9 ; i++){ //passa por todos os elementos do html colocando um "nada"
        document.getElementsByClassName("casa")[i].innerHTML = ""   
    } 
    //---preenche a matriz com numeros aleatórios e diferentes---//
    linha1=["1","2","3"]
    linha2=["4","5","6"]
    linha3=["7","8","9"] 
    //----------------//
    vez = 0 //reset da vez
    njogadas = 0 // reset do numero de jogadas
    jaganhou = false //reset de vitoria
    //---------set inicial de textos do topo---------------------------//
    document.getElementById("indicadorVencedor").style.display= "none"
    document.getElementById("indicadorDaVez").style.display= "block"
    document.getElementById("indicadorDaVez").innerHTML = "É a vez do X"
    //----------------------------------------------------------------//
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
    //---------------------Vitoria por linha--------------------//
    if(linha1[0] == linha1[1]  && linha1[1] == linha1[2]){
        return true
    }
    else if(linha2[0] == linha2[1] && linha2[1] == linha2[2]){
        return true
    } 
    else if(linha3[0] == linha3[1] && linha3[1] == linha3[2] ){
        return true
    } 
    //--------------------Vitoria por coluna-------------------//
    else if(linha1[0] == linha2[0] && linha2[0] == linha3[0]){
        
        return true
    }
    else if(linha1[1] == linha2[1] && linha2[1] == linha3[1]){
        
        return true
    }
    else if(linha1[2] == linha2[2] && linha2[2] == linha3[2]){
        
        return true
    }
    //-------------------Vitoria por diagonal-----------------//
    else if(linha1[0] == linha2[1] && linha2[1] == linha3[2]  ){
        
        return true
    }
    else if(linha1[2] == linha2[1] && linha2[1] == linha3[0]  ){
        
        return true
    }
    //--------------------Ninguem ganhou---------------------//
    else{
        return false
    }
}


    