var i = 0 //variavel que selecionará cada objeto dentro da variavel perguntas
soma = 0 //variável que quardará a pontuação das respostas
somaMaxima = 3*6 // pontuação máxima
//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var comeco = true
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' },
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' },
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' },
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' },
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' },
        ]
    },
    {
        texto: 'Qual o vencedor do último Gamification',
        respostas: [
            { valor: 3, texto: 'Tayllan' },
            { valor: 0, texto: 'Rau' },
            { valor: 1, texto: 'Gustavo' },
            { valor: 2, texto: 'Matheus' },
        ]
    },
]



function mostrarQuestao(){


    if (document.getElementById("alter1").checked || document.getElementById("alter2").checked || document.getElementById("alter3").checked || document.getElementById("alter4").checked || comeco){ //checa se a pergunta foi respondida

        document.getElementById("listaRespostas").style.display = "inline" // deixa a lista de respostas visíveis
        var spans = document.getElementsByTagName("span") // armazena todos os spans

        
        if(i < 6){// enquanto ainda tiver perguntas
            document.getElementById("titulo").innerHTML = perguntas[i].texto //altera o texto da pergunta

            if(i < 5){
                document.getElementById("confirmar").innerHTML = "Proxima" // Deixa o botão com o texto "proxima" até a ultima pergunta
            }
            else{
                document.getElementById("confirmar").innerHTML = "Finalizar Quiz" // Muda para "finalizar" o texto do botão quando for a ultima pergunta
            }    
            
            for(var j = 0; j < spans.length; j++){
                spans[j].innerHTML = perguntas[i].respostas[j].texto// seta a lista de respostas de acordo com as pergunta atual relacionando com a numero de spans
           
            }

            //verificação das alternativas
            if(document.getElementById("alter1").checked){//verifica se a primeira alternativa foi marcada

                soma = soma + (perguntas[i-1].respostas[0].valor) //adciona o respectivo valor da pergunta a soma
            }
            if(document.getElementById("alter2").checked){//verifica se a segunda alternativa foi marcada
                soma = soma + (perguntas[i-1].respostas[1].valor)//adciona o respectivo valor da pergunta a soma
            }
            if(document.getElementById("alter3").checked){//verifica se a terceira alternativa foi marcada
                soma = soma + (perguntas[i-1].respostas[2].valor)//adciona o respectivo valor da pergunta a soma
            }
            if(document.getElementById("alter4").checked){//verifica se a quarta alternativa foi marcada
                soma = soma + (perguntas[i-1].respostas[3].valor)//adciona o respectivo valor da pergunta a soma
            }
            
            document.getElementById("alter1").checked = false//reseta as marcações
            document.getElementById("alter2").checked = false//reseta as marcações
            document.getElementById("alter3").checked = false//reseta as marcações
            document.getElementById("alter4").checked = false//reseta as marcações
        }
        
        if(i >= 6){//quando não tiver perguntas, ou seja, construção a tela de finalização

            //verificação das alternativas para a ultima pergunta
            if(document.getElementById("alter1").checked){//verifica se a primeira alternativa foi marcada
                soma = soma + (perguntas[i-1].respostas[0].valor) //adciona o respectivo valor da pergunta a soma
            }
            if(document.getElementById("alter2").checked){//verifica se a segunda alternativa foi marcada
                soma = soma + (perguntas[i-1].respostas[1].valor)//adciona o respectivo valor da pergunta a soma
            }
            if(document.getElementById("alter3").checked){//verifica se a terceira alternativa foi marcada
                soma = soma + (perguntas[i-1].respostas[2].valor)//adciona o respectivo valor da pergunta a soma
            }
            if(document.getElementById("alter4").checked){//verifica se a quarta alternativa foi marcada
                soma = soma + (perguntas[i-1].respostas[3].valor)//adciona o respectivo valor da pergunta a soma
            }
           
            document.getElementById("confirmar").style.display = "none"// esconde o botão
            document.getElementById("titulo").innerHTML = "O seu resultado é"//muda o titulo
            result = (soma/somaMaxima)*100//calcula a pontuação
            document.getElementById("listaRespostas").innerHTML = result.toFixed(2) + "%";//monstra o resultado
        }

        i++//incremento da variável
        comeco=false
    }      
    

    
}

